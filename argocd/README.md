# How to initialize argocd
## oneliner
`helm dependency build && kc create namespace argocd && helm install argocd . -f values-prod.yaml --namespace argocd`

# Issues and how to solve them

## `Error: INSTALLATION FAILED: template: argo-cd/templates/redis/servicemonitor.yaml:2:18: executing "argo-cd/templates/redis/servicemonitor.yaml" at <.Values.redis.enabled>: nil pointer evaluating interface {}.enabled`
### Status: solved
### Understanding the cause (1-5): 3 
### Overview:
Issue was with redis and its dependency previous files were a mess. Argocd wasn't able to start
### Solution:
Remove all files but `values.yaml`
Rewrite `Chart.yaml` to dependency type chart
Run `helm dependency build`


